import { Component, OnInit } from '@angular/core';
import {NzNotificationService} from 'ng-zorro-antd';

@Component({
  selector: 'app-game-process',
  templateUrl: './game-process.component.html',
  styleUrls: ['./game-process.component.scss']
})
export class GameProcessComponent implements OnInit {

  arrayLetters: any = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
  arrayGrid: any = [];

  arrayAllCellPlayer: any = [];
  arrayAllCellComp: any = [];

  arrayShipsForPlayer1: any = [];
  arrayShipsForPlayer2: any = [];

  arrayShotsPlayer1: any = [];
  arrayShotsPlayer2: any = [];

  arrayDestroyShipsPlayer1: any = [];
  arrayDestroyShipsPlayer2: any = [];

  arrayShotsComputer: any = [];

  isVisible = false;
  isVisibleComplete = false;

  player_one = '';
  player_two = '';

  turnOnShots = '';

  constructor(private notification: NzNotificationService) { }

  ngOnInit() {
    this.showModal();
    this.createArrayGrid();

    this.getAllCellPlayer();
    this.getAllCellComp();

    this.setArrayShipsPlayer(this.arrayShipsForPlayer1);
    this.setArrayShipsComp(this.arrayShipsForPlayer2);
  }

  /** Создание сетки на полях для игры **/

  createArrayGrid(): void {
    const letterArray = this.arrayLetters;
    const allElemArray = this.arrayGrid;
      for (let i = 0; i < 10; i++) {
        allElemArray[i] = [];
          for (let j = 0; j < 10; j++) {
            allElemArray[i][letterArray[j]] = i + 1;
          }
      }
  }

  /** Добавление классов для закрашивания кораблей через (ng-class)**/

  calculateClass(id: any): object {
    const objectClass: object = {
      'shipColorPlayer': this.getPlainShipsPlayer1().findIndex(i => i === id) >= 0,
    };
    return objectClass;
  }

  /** Перебор двумерных массивов кораблей создание простых **/

  getPlainShipsPlayer1(): any {
    const result = [];
    this.arrayShipsForPlayer1.forEach(arr => arr.forEach(a => result.push(a)));
    return result;
  }

  getPlainShipsPlayer2(): any {
    const result = [];
    this.arrayShipsForPlayer2.forEach(arr => arr.forEach(a => result.push(a)));
    return result;
  }

  /** Получение всех координат для полей **/

  getAllCellPlayer(): void {
    const result = [];
      for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 10; j++) {
          result.push((i + 1) + this.arrayLetters[j]);
          this.arrayShotsComputer.push((i + 1) + this.arrayLetters[j]);
        }
      }
    this.arrayAllCellPlayer = result;
  }

  getAllCellComp(): void {
    const result = [];
      for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 10; j++) {
          result.push((i + 1) + this.arrayLetters[j]);
        }
      }
    this.arrayAllCellComp = result;
  }

  /** Создание кораблей (случайным образом) для игрока **/

  setArrayShipsPlayer(shipsArray: any): void {
    const shipArray = shipsArray;
    const battleShip = this.createBattleship(this.arrayAllCellPlayer);
    shipArray.push(battleShip);

    for (let i = 0; i < 2; i ++) {
      const cruiser = this.createCruiser(this.arrayAllCellPlayer);
      shipArray.push(cruiser);
    }

    for (let i = 0; i < 3; i ++) {
      const destroyer = this.createDestroyer(this.arrayAllCellPlayer);
      shipArray.push(destroyer);
    }

    for (let i = 0; i < 4; i ++) {
      const boat = this.createBoat(this.arrayAllCellPlayer);
      shipArray.push(boat);
    }
  }

  /** Создание кораблей (случайным образом) для компьютера **/

  setArrayShipsComp(shipsArray: any): void {
    const shipArray = shipsArray;
    const battleShip = this.createBattleship(this.arrayAllCellComp);
    shipArray.push(battleShip);

    for (let i = 0; i < 2; i ++) {
      const cruiser = this.createCruiser(this.arrayAllCellComp);
      shipArray.push(cruiser);
    }

    for (let i = 0; i < 3; i ++) {
      const destroyer = this.createDestroyer(this.arrayAllCellComp);
      shipArray.push(destroyer);
    }

    for (let i = 0; i < 4; i ++) {
      const boat = this.createBoat(this.arrayAllCellComp);
      shipArray.push(boat);
    }
  }

  /** Создание линкора **/

  createBattleship (arrayCellIntro: any): any {
    let randNum = Math.floor(Math.random() * this.arrayGrid.length);
      if (randNum === 0) {
        randNum += 1;
      }
    let numberOfLetter = Math.floor(Math.random() * this.arrayGrid.length);
      if (numberOfLetter === 0) {
        numberOfLetter += 1;
      }
    const randLetter = this.arrayLetters[numberOfLetter];
    const battleship = [];

    let arrayCell = [];
    arrayCell = arrayCellIntro;

    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {
        if (i === randNum && randLetter === this.arrayLetters[j]) {
          battleship[0] = randNum + randLetter;
            let count = randNum;

            if (this.arrayGrid[i + 3] !== undefined) {
              for (let k = 0; k < 3; k++) {
                  battleship[k + 1] = (count + 1) + randLetter;
                  count += 1;
                }
            } else if (this.arrayGrid[i - 4] !== undefined) {
                for (let k = 0; k < 3; k++) {
                  battleship[k + 1] = (count - 1) + randLetter;
                  count -= 1;
                }
            } else if (this.arrayLetters[numberOfLetter + 3] !== undefined) {
                for (let k = 0; k < 3; k++) {
                  battleship[k + 1] = randNum + this.arrayLetters[count + 1];
                  count += 1;
                }
            } else if (this.arrayLetters[numberOfLetter - 4] !== undefined) {
                  for (let k = 0; k < 3; k++) {
                    battleship[k + 1] = randNum + this.arrayLetters[count - 1];
                    count -= 1;
                  }
              }

            /** Удаление занятых координат **/

            battleship.forEach(function (item) {
              const indexEl = arrayCell.indexOf(item);
              arrayCell.splice(indexEl, 1);
            });

          return battleship;
        }
      }
    }
  }

  /** Создание крейсера **/

  createCruiser(arrayCellIntro: any): any {
    let randNum = Math.floor(Math.random() * this.arrayGrid.length); /** Получение случайной клетки по цифре **/
      if (randNum === 0) {
        randNum += 1;
      }
    let numberOfLetter = Math.floor(Math.random() * this.arrayGrid.length); /** Получение случайной клетки по букве **/
      if (numberOfLetter === 0) {
        numberOfLetter += 1;
      }

    const randLetter = this.arrayLetters[numberOfLetter];
    const cruiser = [];

    let arrayCell = [];
    arrayCell = arrayCellIntro;

    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {
        if (i === randNum && randLetter === this.arrayLetters[j]) {
          let count = randNum;

          const find = arrayCell.find(function (element) {
            return element === randNum + randLetter;
          });

            if (find !== undefined) {
              if (this.arrayGrid[i + 2] !== undefined
                && arrayCell.indexOf((randNum + 1) + this.arrayLetters[j]) !== -1
                && arrayCell.indexOf((randNum + 2) + this.arrayLetters[j]) !== -1
                && arrayCell.indexOf((randNum + 3) + this.arrayLetters[j]) !== -1) {
                  cruiser[0] = randNum + randLetter;
                    for (let k = 0; k < 2; k++) {
                      cruiser[k + 1] = (count + 1) + randLetter;
                      count += 1;
                    }
              } else if (this.arrayGrid[i - 3] !== undefined
                && arrayCell.indexOf((randNum - 1) + this.arrayLetters[j]) !== -1
                && arrayCell.indexOf((randNum - 2) + this.arrayLetters[j]) !== -1
                && arrayCell.indexOf((randNum - 3) + this.arrayLetters[j]) !== -1
                && arrayCell.indexOf((randNum - 4) + this.arrayLetters[j]) !== -1) {
                  cruiser[0] = randNum + randLetter;
                    for (let k = 0; k < 2; k++) {
                      cruiser[k + 1] = (count - 1) + randLetter;
                      count -= 1;
                    }
              } else if (this.arrayLetters[j + 2] !== undefined
                && arrayCell.indexOf(randNum + this.arrayLetters[j + 1]) !== -1
                && arrayCell.indexOf(randNum + this.arrayLetters[j + 2]) !== -1
                && arrayCell.indexOf(randNum + this.arrayLetters[j + 3]) !== -1) {
                  cruiser[0] = randNum + randLetter;
                    for (let k = 0; k < 2; k++) {
                      cruiser[k + 1] = randNum + this.arrayLetters[count + 1];
                      count += 1;
                    }
              } else if (this.arrayLetters[j - 3] !== undefined
                && arrayCell.indexOf(randNum + this.arrayLetters[j - 1]) !== -1
                && arrayCell.indexOf(randNum + this.arrayLetters[j - 2]) !== -1
                && arrayCell.indexOf(randNum + this.arrayLetters[j - 3]) !== -1
                && arrayCell.indexOf(randNum + this.arrayLetters[j - 4]) !== -1) {
                  cruiser[0] = randNum + randLetter;
                    for (let k = 0; k < 2; k++) {
                      cruiser[k + 1] = randNum + this.arrayLetters[count - 1];
                      count -= 1;
                    }
              }
            } else {
              const save = Math.floor(Math.random() * arrayCell.length);
              cruiser.push(arrayCell[save]);
              cruiser.push(arrayCell[save + 1]);
              cruiser.push(arrayCell[save + 2]);
            }

          /** Удаление занятых координат **/

          cruiser.forEach(function (item) {
            const indexEl = arrayCell.indexOf(item);
            arrayCell.splice(indexEl, 1);
          });

          return cruiser;
        }
      }
    }
  }

  /** Создание эсминца **/

  createDestroyer(arrayCellIntro: any): any {
    let randNum = Math.floor(Math.random() * this.arrayGrid.length);
      if (randNum === 0) {
        randNum += 1;
      }
    let numberOfLetter = Math.floor(Math.random() * this.arrayGrid.length);
      if (numberOfLetter === 0) {
        numberOfLetter += 1;
      }
    const randLetter = this.arrayLetters[numberOfLetter];
    const destroyer = [];

    let arrayCell = [];
    arrayCell = arrayCellIntro;

    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {
        if (i === randNum && randLetter === this.arrayLetters[j]) {
          let count = randNum;

          const find = arrayCell.find(function (element) {
            return element === randNum + randLetter;
          });

            if (find !== undefined) {
                if (this.arrayGrid[i + 1] !== undefined
                  && arrayCell.indexOf((randNum + 1) + this.arrayLetters[j]) !== -1
                  && arrayCell.indexOf((randNum + 2) + this.arrayLetters[j]) !== -1) {
                  destroyer[0] = randNum + randLetter;
                    for (let k = 0; k < 1; k++) {
                      destroyer[k + 1] = (count + 1) + randLetter;
                      count += 1;
                    }
                } else if (this.arrayGrid[i - 2] !== undefined
                  && arrayCell.indexOf((randNum - 1) + this.arrayLetters[j]) !== -1
                  && arrayCell.indexOf((randNum - 2) + this.arrayLetters[j]) !== -1
                  && arrayCell.indexOf((randNum - 3) + this.arrayLetters[j]) !== -1) {
                    destroyer[0] = randNum + randLetter;
                      for (let k = 0; k < 1; k++) {
                        destroyer[k + 1] = (count - 1) + randLetter;
                        count -= 1;
                      }
                } else if (this.arrayLetters[j + 1] !== undefined
                  && arrayCell.indexOf(randNum + this.arrayLetters[j + 1]) !== -1
                  && arrayCell.indexOf(randNum + this.arrayLetters[j + 2]) !== -1) {
                    destroyer[0] = randNum + randLetter;
                      for (let k = 0; k < 1; k++) {
                        destroyer[k + 1] = randNum + this.arrayLetters[count + 1];
                        count += 1;
                      }
                } else if (this.arrayLetters[j - 2] !== undefined
                  && arrayCell.indexOf(randNum + this.arrayLetters[j - 1]) !== -1
                  && arrayCell.indexOf(randNum + this.arrayLetters[j - 2]) !== -1
                  && arrayCell.indexOf(randNum + this.arrayLetters[j - 3]) !== -1) {
                    destroyer[0] = randNum + randLetter;
                      for (let k = 0; k < 1; k++) {
                        destroyer[k + 1] = randNum + this.arrayLetters[count - 1];
                        count -= 1;
                      }
                }
            } else {
              const save = Math.floor(Math.random() * arrayCell.length);
              destroyer.push(arrayCell[save]);
              destroyer.push(arrayCell[save + 1]);
            }

          /** Удаление занятых координат **/

          destroyer.forEach(function (item) {
            const indexEl = arrayCell.indexOf(item);
            arrayCell.splice(indexEl, 1);
          });

          return destroyer;
        }
      }
    }
  }

  /** Создание катера **/

  createBoat(arrayCellIntro: any): any {
    let arrayCell = [];
    arrayCell = arrayCellIntro;

    const boatEl = Math.floor(Math.random() * arrayCell.length);
    const boat = [arrayCell[boatEl]];

    /** Удаление занятых координат **/

    boat.forEach(function (item) {
      const indexEl = arrayCell.indexOf(item);
      arrayCell.splice(indexEl, 1);
    });

    return boat;
  }

  /** Ход компьютера **/

  moveComputer(): void {
    const element = Math.floor(Math.random() * this.arrayShotsComputer.length); /** Массив выстрелов компьютера, чтобы два раза не бил в
     одну клетку**/
    const move = this.arrayShotsComputer[element];

    this.arrayShotsComputer.splice(element, 1);

    const _this = this;
    setTimeout(function () {
      _this.clickOnTile(move);
    }, 1500);
  }

  /** Клик по полю **/

  clickOnTile(id: any): any {
    const elementId = document.getElementById(id);
    const player1 = this.getPlainShipsPlayer1().findIndex(i => i === id) >= 0; /** Проверка клика по полю игрока **/
    const player2 = this.getPlainShipsPlayer2().findIndex(i => (i + 1) === id) >= 0; /** Проверка клика по полю компьютера **/

    const playerShips = this.getPlainShipsPlayer1(); /** Получение всех кораблей игрока **/
    const computerShips = this.getPlainShipsPlayer2(); /** Получение всех кораблей компьютера **/

    /** Подбитый корабль Игрока1 **/

    if (player1 === true) {
      elementId.style.background = 'red';
      elementId.style.transform = 'rotateY(360deg)';

      this.arrayDestroyShipsPlayer1.push(id);
      this.arrayShotsPlayer2.push(id);

      if (this.arrayDestroyShipsPlayer1.length === playerShips.length) {
        this.showComplete();
      }

      this.turnOnShots = this.player_two;
        this.notification.blank(
          'Подбит!',
          'Игрок2 попал по кораблю Игрока1, дополнительный ход Игрока2.'
        );
      this.moveComputer();
    }

    /** Подбитый корабль Игрока2 **/

    if (player2 === true) {
      elementId.style.background = 'red';
      elementId.style.transform = 'rotateY(360deg)';

      this.arrayDestroyShipsPlayer2.push(id);
      this.arrayShotsPlayer1.push(id);

      if (this.arrayDestroyShipsPlayer2.length === computerShips.length) {
        this.showComplete();
      }

      this.turnOnShots = this.player_one;
        this.notification.blank(
          'Подбит!',
          'Игрок1 вы попали по кораблю, у Вас дополнительный ход.'
        );
    }

    /** Выстрелы Игроков по полям без корабля **/

    if (player1 === false && player2 === false) {
      elementId.style.background = 'gray';

      if (id.length === 2) {
        this.arrayShotsPlayer2.push(id);
          this.notification.blank(
            'Мимо!',
            'Игрок2 не попал,ход Игрока1.'
          );
        this.turnOnShots = this.player_one;
      } else if (id.length === 3) {
        this.arrayShotsPlayer1.push(id);
          this.notification.blank(
            'Мимо!',
            'К сожалению вы не попали,ход Игрока2.'
          );
        this.turnOnShots = this.player_two;
        this.moveComputer();
      }
    }
  }

  /** Для модального окна с заполнением имен **/

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    if (this.player_one === '' || this.player_two === '') {
      this.notification.blank(
        'Предупреждение',
        'Введите имена игроков'
      );
    } else {
      this.turnOnShots = this.player_one;
      this.isVisible = false;
    }
  }

  handleCancel(): void {
    if (this.player_one === '' || this.player_two === '') {
      this.notification.blank(
        'Предупреждение',
        'Введите имена игроков'
      );
    } else {
      this.isVisible = false;
    }
  }

  /** Для модального окна - завершения игры **/

  showComplete(): void {
    this.isVisibleComplete = true;
  }

  completeClose(): void {
    this.isVisibleComplete = false;
    document.location.reload(true);
  }
}
