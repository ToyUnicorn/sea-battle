import { NgModule } from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {IntroductionComponent} from './introduction/introduction.component';
import {GameProcessComponent} from './game-process/game-process.component';

const routes: Routes = [
  { path: '', component: IntroductionComponent},
  { path: 'sea-battle', component: GameProcessComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}
